#!/bin/sh -eux
exec zip -r rpt.xpi . -x rpt.xpi -x buildxpi.sh -x ".git/*"
